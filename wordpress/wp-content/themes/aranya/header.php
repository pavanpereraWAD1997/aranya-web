<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Aranya Kosgoda Sri Lanka</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- animation -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <!-- swiper -->
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css" />
    <!-- stylesheet -->
    <link rel='stylesheet' type='text/css' media='screen' href='<?php bloginfo('template_directory'); ?>/css/style.css'>

    <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.png" />
</head>
