// Main Slider

var swiper = new Swiper('.main-slider', {
    spaceBetween: 30,
    centeredSlides: true,
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    loop: true,
    loopFillGroupWithBlank: true,
    effect: 'fade',
    pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
    },
});

// second-slider

var swiper = new Swiper('.second-slider', {
    spaceBetween: 30,
    centeredSlides: true,
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    loop: true,
    loopFillGroupWithBlank: true,
    effect: 'fade',
    pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
    },
});


// third-slider

var swiper = new Swiper('.third-slider', {
    spaceBetween: 30,
    centeredSlides: true,
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    loop: true,
    loopFillGroupWithBlank: true,
    effect: 'fade',
    pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
    },
});