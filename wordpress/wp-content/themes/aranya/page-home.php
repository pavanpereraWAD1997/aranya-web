<?php

/**
 * Template Name: Home
 */
get_header();

?>

<body id="home">


<section id="preloader" class="preloader">
        <div class="box_section">
            <div class="image_box">
              <img alt="image_preloader"src="<?php bloginfo('template_directory'); ?>/images/logo.png" class="site_logo"/>
            </div>
            <div class="img_filter"></div>
        </div>
 </section>



    <section id="fix-remove" class="view-height-section">
        <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" class="logo-top" data-aos="fade"></a>
        <div class="container-fluid">
            <div class="row">

                <!-- Swiper -->
                <div class="swiper-container main-slider">
                    <div class="swiper-wrapper">

                    <?php if( have_rows('home_banner') ): ?>
                        <?php while ( have_rows('home_banner') ) : the_row();?>

                        <div class="swiper-slide">
                            <img src="<?php the_sub_field('home_banner_image'); ?>" class="full-imag-size">
                        </div>

                        <?php endwhile; ?>
                    <?php endif; ?>  


                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>


                </div>
            </div>

        </div>

    </section>


    <section>
        <div class="container-fluid">
            <div class="row top-back">
                <!-- illustration -->
                <img src="<?php bloginfo('template_directory'); ?>/images/menu.png" class="top-shade">
                <img src="<?php bloginfo('template_directory'); ?>/images/1-1.jpg" class="top-illus">

                <!-- menu -->
                <nav id="verschwinden" id="verschwinden" class="navbar navbar-expand-lg navbar-light bg-spcl ">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                        aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="navbar-collapse collapse justify-content-center order-2" id="navbarText">

                    <?php wp_nav_menu(array('menu' => 'main-menu', 'menu_class' => 'navbar-nav navbar-center ')); ?>
<!-- 
                        <ul class="navbar-nav navbar-center">
                            <li class="nav-item ">
                                <a class="nav-link" href="about.html">About Us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="lodging.html">Lodging</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="food.html">food</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="roots.html">roots</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contact.html">Contact Us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="gallery.html">Gallery</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="thing.html">Things to do</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Book Now</a>
                            </li>
                        </ul> -->

                    </div>
                </nav>
            </div>
        </div>

        <!-- content -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="title-main marg" data-aos="fade">
                    <?php the_field('main_heading'); ?>
                    </div>
                    <div class="desc-main" data-aos="fade" data-aos-duration="2500">
                        
                    <?php the_field('content_1'); ?>

                    </div>
                </div>

                <div class="col-12 second-slider-sec">
                    <!-- Swiper -->
                    <div class="swiper-container second-slider">
                        <div class="swiper-wrapper">

                        <?php if( have_rows('slider_2') ): ?>
                        <?php while ( have_rows('slider_2') ) : the_row();?>
                            <div class="swiper-slide">
                                <img src="<?php the_sub_field('slider2_image'); ?>" class="fullsize-img shadow-img">
                            </div>

                        <?php endwhile; ?>
                        <?php endif; ?>  


                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class="float-section">
        <div class="container-fluid p-0">
            <div class="row bg-brown">
                <img src="<?php bloginfo('template_directory'); ?>/images/3.jpg" class="ilus-3">
                <div class="col-12 cont-sec">
                    <div class="title-main2" data-aos="fade">
                        ABOUT US
                    </div>
                    <div class="desc-main2" data-aos="fade" data-aos-duration="2500">
                       

                        <?php the_field('about_content'); ?>

                    </div>

                </div>

            </div>
        </div>
    </section>
    <section>
        <div class="container-fluid p-0">
            <div class="row ">
                <img src="<?php bloginfo('template_directory'); ?>/images/4.jpg" class="ilus-3">
                <img src="<?php bloginfo('template_directory'); ?>/images/chair.png" class="chair">
                <div class="cont-sec2-1 float-lodgin">
                    <div class="title-main mr-t" data-aos="fade">
                        VILLAS
                    </div>
                    <div class="desc-main" data-aos="fade" data-aos-duration="2500">
                    <?php the_field('lodging_content'); ?>




                    </div>
                </div>
                <div class="col-12 cont-sec2">
                    <!-- Swiper -->
                    <div class="swiper-container third-slider">
                        <div class="swiper-wrapper">

                        <?php
	                    $loop = new WP_Query( array( 'post_type' => 'rooms', 'posts_per_page' => -1 ) );  
	                    $postCount = $loop->post_count;
                        while ( $loop->have_posts() ) : $loop->the_post(); ?>

                            <div class="swiper-slide row">
                                <div class="col-lg-7 col-md-7 col-12 p-0 mob-show">
                                    <img src="<?php the_field('room_image_home'); ?>" class="fullsize-img shadow-img">
                                </div>
                                <div class="col-lg-5 col-md-5 col-12 p-0">
                                    <div class="bg-brown float-slide-part">
                                        <div class="desc-main sp-col" data-aos="fade" data-aos-duration="2500">
                                            <span><?php the_title();?></span><br>
                                            <?php the_field('display_content_short'); ?>
                                        </div>
                                        <div class="Aran-btn " data-aos="fade" data-aos-duration="2500">
                                            <a href="<?php the_permalink();?>">DISCOVER MORE</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-12 p-0 mob-remove">
                                    <img src="<?php the_field('room_image_home'); ?>" class="fullsize-img shadow-img">
                                </div>

                            </div>

                        <?php endwhile; wp_reset_query(); ?>

                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                    </div>

                </div>


            </div>
        </div>
    </section>

    <section>
        <div class="container-fluid">
            <div class="row">
                <img src="<?php bloginfo('template_directory'); ?>/images/5.jpg" class="fish-illus">


                <div class="col-lg-7 col-md-6 col-12 p-0">
                    <img src="<?php bloginfo('template_directory'); ?>/images/fishback.jpg" class="sp-size-img shadow-img">
                    <img src="<?php bloginfo('template_directory'); ?>/images/back-box.png" class="back-box">
                 <?php /*   <img src="<?php bloginfo('template_directory'); ?>/images/fish.png" class="fish">
                    <img src="<?php bloginfo('template_directory'); ?>/images/fish-shade.png" class="fish shade"> */ ?>

                </div>
                <div class="col-lg-5 col-md-6 col-12 pad-remove">
                    <div class="cont-sec2-1 ">
                        <div class="title-main mr-t" data-aos="fade">
                            CUISINE
                        </div>
                        <div class="desc-main" data-aos="fade" data-aos-duration="2500">
                            
                            <?php the_field('food_fare_content'); ?>

                        </div>
                        <div class="Aran-btn " data-aos="fade" data-aos-duration="2500">
                            <a href="<?php echo home_url(); ?>/food">DISCOVER MORE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 break-pad">
                    <img src="<?php the_field('beach_image'); ?>" class="fullsize-img shadow-img">
                    <img src="<?php bloginfo('template_directory'); ?>/images/beach-back.png" class="fullsize-img back-beach">

                </div>
            </div>
        </div>
    </section>
    <section class="adjst-pad">
        <div class="container-fluid">
            <div class="row">
                <img src="<?php bloginfo('template_directory'); ?>/images/1-1.jpg" class="top-illus2">
                <div class="col-12">
                    <div class="title-main" data-aos="fade">
                        ARANYA'S ROOTS
                    </div>
                    <div class="desc-main roots" data-aos="fade" data-aos-duration="2500">
                    <?php the_field('roots_content'); ?>
     
                    </div>
                    <div class="Aran-btn " data-aos="fade" data-aos-duration="2500">
                        <a href="<?php echo home_url(); ?>/roots">EXPLORE</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="boat-sec adjst-pad2">
        <div class="container-fluid">
            <div class="row">
                <img src="<?php bloginfo('template_directory'); ?>/images/7.jpg" class="top-illus3">

                <div class="col-lg-5 col-md-5 col-12 order-2 order-md-1">
                    <img src="<?php bloginfo('template_directory'); ?>/images/line-2.png" class="line3">

                    <div class="cont-sec2-1 ">
                        <div class="title-main mr-t" data-aos="fade">
                            DIVE DEEP
                        </div>
                        <div class="desc-main" data-aos="fade" data-aos-duration="2500">
                            
                            <?php the_field('dive_deep_content'); ?>
                        </div>
                        <div class="Aran-btn " data-aos="fade" data-aos-duration="2500">
                            <a href="<?php echo home_url(); ?>/things-to-do">EXPLORE</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-12 order-1 order-md-2 p-0">
                    <img src="<?php the_field('dive_deep_boat'); ?>" class="sp-size-img shadow-img">
                    <img src="<?php bloginfo('template_directory'); ?>/images/back-box.png" class="back-box2">
                    <img src="<?php bloginfo('template_directory'); ?>/images/line-2.png" class="line1">
                    <img src="<?php bloginfo('template_directory'); ?>/images/line-2.png" class="line2">

                </div>
            </div>
        </div>
    </section>




<?php get_footer(); ?>

