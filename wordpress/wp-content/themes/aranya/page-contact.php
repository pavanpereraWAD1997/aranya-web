<?php

/**
 * Template Name: Contact
 */
get_header();

?>

<body>
<section id="preloader" class="preloader">
        <div class="box_section">
            <div class="image_box">
              <img alt="image_preloader"src="<?php bloginfo('template_directory'); ?>/images/logo.png" class="site_logo"/>
            </div>
            <div class="img_filter"></div>
        </div>
 </section>

    <section>
        <?php /* <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" class="logo-top" data-aos="fade"></a> */ ?>
        <div class="container-fluid">
            <div class="row">

                <div class="col-12 p-0">
                    <img src="<?php the_field('banner'); ?>" class="fullsize-img banner-inner">
                </div>

            </div>
        </div>

        </div>

    </section>
    <section>
        <div class="container-fluid">
            <div class="row top-back">
                <!-- illustration -->
                <img src="<?php bloginfo('template_directory'); ?>/images/1-1.jpg" class="top-illus">

                <!-- menu -->
                <nav id="verschwinden" class="navbar navbar-expand-lg navbar-light bg-spcl">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                        aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="navbar-collapse collapse justify-content-center order-2" id="navbarText">
                    <?php wp_nav_menu(array('menu' => 'main-menu', 'menu_class' => 'navbar-nav navbar-center ')); ?>
                    </div>
                </nav>
            </div>
        </div>

        <!-- content -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-12 inner-content cont-pandding">
                    <div class="title-main" data-aos="fade">
                        CONTACT US
                    </div>
                    <div class="desc-main" data-aos="fade" data-aos-duration="2500">
                        <?php the_field('intro'); ?>
                        
                        Contact - <a href="tel: <?php the_field('footer_telephone', 'option'); ?>"><?php the_field('footer_telephone', 'option'); ?></a> | email - <a
                            href="mailto: <?php the_field('footer_email', 'option'); ?>"><?php the_field('footer_email', 'option'); ?></a> |
                        <a href="<?php the_field('footer_website', 'option'); ?>"><?php the_field('footer_website', 'option'); ?></a>
                    </div>
                </div>

                <div class="col-12 inner-slider  map"  >
                    <div class="brown-mask bg-brown "></div>
                    <?php the_field('map'); ?>
                </div>
            </div>
        </div>

    </section>
    <?php get_footer(); ?>