// preload

function preloader() {
    document.getElementById("preloader").style.display = "none";
}//preloader

//remove/comment this one to check the website in loading state
window.onload = preloader;

// Main Slider

var swiper = new Swiper('.main-slider', {
    spaceBetween: 30,
    centeredSlides: true,
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    loop: true,
    loopFillGroupWithBlank: true,
    effect: 'fade',
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
});

// second-slider

var swiper = new Swiper('.second-slider', {
    spaceBetween: 30,
    centeredSlides: true,
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    loop: true,
    loopFillGroupWithBlank: true,
    effect: 'fade',
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
});


// third-slider

var swiper = new Swiper('.third-slider', {
    spaceBetween: 30,
    centeredSlides: true,
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    loop: true,
    loopFillGroupWithBlank: true,
    effect: 'fade',
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
});

// second-slider2

var swiper = new Swiper('.second-slider2', {
    spaceBetween: 30,
    centeredSlides: true,
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    loop: true,
    loopFillGroupWithBlank: true,
    effect: 'fade',
    pagination: {
        el: '.swiper-pagination',
        clickable: true,

    },
});


// second-slider3

var swiper = new Swiper('.second-slider3', {
    spaceBetween: 30,
    centeredSlides: true,
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    loop: true,
    loopFillGroupWithBlank: true,
    effect: 'fade',
    pagination: {
        el: '.swiper-pagination',
        clickable: true,

    },
});


