<footer>
        <div class="container-fluid p-0">
            <div class="row bg-brown footer">
                <div class="col-lg-4 col-md-4 col-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5608.142375629156!2d80.03008099333319!3d6.321963872623053!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4d599645c16b3574!2sAranya%20Beach%20Bungalows!5e0!3m2!1sen!2slk!4v1614589902542!5m2!1sen!2slk" width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
                <div class="col-lg-2 col-md-2 col-12 center-pc">
                    <div class="footer-head">
                        <?php the_field('footer_set_1', 'option'); ?>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-12 center-pc">

                    <div class="footer-head">
                    <br>
                        <span>
                            Tel - <a href="tel:<?php the_field('footer_telephone', 'option'); ?>"><?php the_field('footer_telephone', 'option'); ?><a>
                        </span>
                        <br>
                        email - <a href="mailto:<?php the_field('footer_email', 'option'); ?>"><?php the_field('footer_email', 'option'); ?></a><br>
                        <a href="<?php the_field('footer_website', 'option'); ?>"><?php the_field('footer_website', 'option'); ?>

                    </a>
                    <br>
                    &nbsp
                    </div>
                </div>
                <div class="col-lg-1 col-md-2 col-12 center-pc">
                    <div class="footer-head">
                    <br>

                        <span>
                            FIND US ON
                        </span>
                        <br>
                        <a href="<?php the_field('instergram', 'option'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/instagram.svg" class="social"></a>
                        <a href="<?php the_field('facebook', 'option'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/facebook.svg" class="social">

                        </a>
                        <br>
                        &nbsp


                    </div>
                </div>
                <div class="col-lg-3 col-md-12 col-12 center-pc">
                    <ul class="footer-menu-li">
                        <li><a href="<?php echo home_url(); ?>">Home</li>
                        <li><a href="<?php echo home_url(); ?>/tissa">Lodging</li>
                        <li><a href="<?php echo home_url(); ?>/food">Food</li>
                        <li><a href="<?php echo home_url(); ?>/roots">Roots</li>
                    </ul>
                    <ul class="footer-menu-li">
                        <li><a href="<?php echo home_url(); ?>/contact-us">Contact Us</li>
                        <li><a href="<?php echo home_url(); ?>/gallery">Gallery</li>
                        <li><a href="<?php echo home_url(); ?>/things-to-do">Experiences</li>
                        <li><a href="<?php echo home_url(); ?>">Book Now</li>

                    </ul>
                    <div class="concept">
                        Concept and Design Bayroo
                    </div>
                </div>
            </div>
        </div>

    </footer>

</body>
<!-- swiper -->
<script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
<!-- animation -->
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<!-- bootstrap -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>

<!-- js -->
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>

<script>
    AOS.init({
        duration: 2000,
    })
</script>

<script>
    
    jQuery(function ($) {
    
    var $nav = $('#verschwinden');
    var $win = $(window);
    var winH = $win.height();   // Get the window height.
    
    $win.on("scroll", function () {
        if ($(this).scrollTop() > winH) {
            $nav.addClass("fixed-top");
        } else {
            $nav.removeClass("fixed-top");
        }
    }).on("resize", function () { // If the user resizes the window
        winH = $(this).height(); // you'll need the new height value
    });
    
    });
    </script>

</html>