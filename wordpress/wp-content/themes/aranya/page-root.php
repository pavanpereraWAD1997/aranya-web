<?php

/**
 * Template Name: Root
 */
get_header();

?>

<body>
<section id="preloader" class="preloader">
        <div class="box_section">
            <div class="image_box">
              <img alt="image_preloader"src="<?php bloginfo('template_directory'); ?>/images/logo.png" class="site_logo"/>
            </div>
            <div class="img_filter"></div>
        </div>
 </section>

    <section class="gray-back">
        <?php /* <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" class="logo-top" data-aos="fade"></a> */ ?>
        <div class="container-fluid">
            <div class="row">

                <div class="col-12 p-0">
                    <img src="<?php the_field('banner'); ?>" class="fullsize-img banner-inner">
                </div>

            </div>
        </div>

        </div>

    </section>
    <section class="gray-back">
        <div class="container-fluid">
            <div class="row top-back">
                <!-- illustration -->
                <img src="<?php bloginfo('template_directory'); ?>/images/menu.png" class="top-shade">
                <img src="<?php bloginfo('template_directory'); ?>/images/1-1.jpg" class="top-illus">

                <!-- menu -->
                <nav id="verschwinden" class="navbar navbar-expand-lg navbar-light bg-spcl">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                        aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="navbar-collapse collapse justify-content-center order-2" id="navbarText">
                    <?php wp_nav_menu(array('menu' => 'main-menu', 'menu_class' => 'navbar-nav navbar-center ')); ?>
                    </div>
                </nav>
            </div>
        </div>

        <!-- content -->

        <div class="container-fluid">
            <div class="row pad-top-break">
            <img src="<?php bloginfo('template_directory'); ?>/images/4.jpg" class="ilus-3 sp-llll2">

                <div class="col-12 inner-content mr-b">
                    <div class="title-main" data-aos="fade">
                    <?php the_field('title'); ?>
                    </div>
                    <div class="desc-main" data-aos="fade" data-aos-duration="2500">
                    <?php the_field('intro'); ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-12 p-0 mob-show abt"  >
                    <img src="<?php the_field('image'); ?>" class="fullsize-img height-change shadow-img">
                </div>
                <div class="col-lg-6 col-md-12 col-12 sp-pad-bg abt">
                    <div class="bg-brown">
                        <div class="cont-sec2-1 ">

                        
                            <div class="title-main" data-aos="fade">
                            <?php the_field('title_1'); ?>
                            </div>

                            <div class="desc-main" data-aos="fade" data-aos-duration="2500">
                            <?php the_field('content'); ?>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-12 p-0 mob-remove abt"  >
                    <img src="<?php the_field('image'); ?>" class="fullsize-img height-change shadow-img">
                </div>
                <div class="col-lg-6 col-md-12 col-12 p-0  abt"  >
                    <img src="<?php the_field('image2'); ?>" class="fullsize-img height-change shadow-img">
                </div>
                <div class="col-lg-6 col-md-12 col-12 sp-pad-bg abt ">
                    <div class="bg-brown">
                        <div class="cont-sec2-1 ">

                            <div class="title-main" data-aos="fade">
                            <?php the_field('title_2'); ?>
                            </div>

                            <div class="desc-main" data-aos="fade" data-aos-duration="2500">
                            <?php the_field('content2'); ?>
                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>

    </section>

<?php get_footer(); ?>