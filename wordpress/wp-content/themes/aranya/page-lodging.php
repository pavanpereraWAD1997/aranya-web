<?php

/**
 * Template Name: lodging
 */
get_header();

?>

<body>
<section id="preloader" class="preloader">
        <div class="box_section">
            <div class="image_box">
              <img alt="image_preloader"src="<?php bloginfo('template_directory'); ?>/images/logo.png" class="site_logo"/>
            </div>
            <div class="img_filter"></div>
        </div>
 </section>

    <section>
        <?php /* <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" class="logo-top" data-aos="fade"></a> */ ?>
        <div class="container-fluid">
            <div class="row">

                <div class="col-12 p-0">
                    <img src="<?php the_field('banner'); ?>" class="fullsize-img banner-inner">
                </div>

            </div>
        </div>

        </div>

    </section>
    <section>
        <div class="container-fluid">
            <div class="row top-back">
                <!-- illustration -->
                <img src="<?php bloginfo('template_directory'); ?>/images/menu.png" class="top-shade">
                <img src="<?php bloginfo('template_directory'); ?>/images/1-1.jpg" class="top-illus">

                <!-- menu -->
                <nav id="verschwinden" class="navbar navbar-expand-lg navbar-light bg-spcl">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                        aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="navbar-collapse collapse justify-content-center order-2" id="navbarText">
                    <?php wp_nav_menu(array('menu' => 'main-menu', 'menu_class' => 'navbar-nav navbar-center ')); ?>
                    </div>
                </nav>
            </div>
        </div>

        <!-- content -->

        <div class="container-fluid">
            <div class="row  pad-top-break">
                    <div class="col-12 inner-content mr-b">
                        <div class="title-main" data-aos="fade">
                        <?php the_field('lodge_title'); ?>
                        </div>
                        <div class="desc-main" data-aos="fade" data-aos-duration="2500">
                        <?php the_content();?>
                        </div>
                    </div>
            <?php
            $i = 1;
	        $loop = new WP_Query( array( 'post_type' => 'rooms', 'posts_per_page' => -1 ) );  
	        $postCount = $loop->post_count;
            while ( $loop->have_posts() ) : $loop->the_post();  ?>

                <?php if($i % 2 == 1): 
                $i++   
                    ?>
                <div class="col-lg-6 col-md-12 col-12 sp-pad-bg" >
                    <div class="bg-brown">
                        <div class="cont-sec2-1 ">
                            <div class="title-main mr-t mr-b" data-aos="fade">
                                <?php the_field('display_name'); ?>
                            </div>
                            <div class="desc-main" data-aos="fade" data-aos-duration="2500">
                                <?php the_field('display_content'); ?>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-12 p-0"  >

                  <!-- Swiper -->
                  <div class="swiper-container second-slider2 shadow-img">
                        <div class="swiper-wrapper">

                        <?php if( have_rows('room_images') ): ?>
                        <?php while ( have_rows('room_images') ) : the_row();?>

                            <div class="swiper-slide room-img">
                            <img src="<?php the_sub_field('room_image2'); ?>" class="fullsize-img height-change">
                            </div>

                        <?php endwhile; ?>
                        <?php endif; ?>


                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                    </div>

                    
                </div>

                <?php else: 
                    $i++   
                    ?>

                    
                <div class="col-lg-6 col-md-12 col-12 p-0"  >
                <!-- Swiper -->
                  <div class="swiper-container second-slider2 shadow-img">
                        <div class="swiper-wrapper">

                        <?php if( have_rows('room_images') ): ?>
                        <?php while ( have_rows('room_images') ) : the_row();?>

                            <div class="swiper-slide room-img">
                            <img src="<?php the_sub_field('room_image2'); ?>" class="fullsize-img height-change">
                            </div>

                        <?php endwhile; ?>
                        <?php endif; ?>


                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-12 sp-pad-bg">
                    <div class="bg-brown">
                        <div class="cont-sec2-1 ">
                            <div class="title-main mr-t mr-b" data-aos="fade">
                                <?php the_field('display_name'); ?>
                            </div>
                            <div class="desc-main" data-aos="fade" data-aos-duration="2500">
                                <?php the_field('display_content'); ?>
                            </div>

                        </div>
                    </div>
                </div>


                <?php endif; ?>

            <?php endwhile; wp_reset_query(); ?>    

            </div>
        </div>

    </section>

    <?php get_footer(); ?>